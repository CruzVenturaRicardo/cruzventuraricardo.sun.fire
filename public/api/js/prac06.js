var Aplicacion = (
  function() {
    var _RECURSO = 'http://cruzventuraricardo.sun.fire2/xml';

    var _mostrarMensaje = function(mensaje) {
      var ventana = document.getElementById('ventanaMensaje');
      if (ventana.firstChild) {
        ventana.removeChild(ventana.firstChild);
      }

      var panel_error = document.createElement('div');
      panel_error.className = "panel " + mensaje.tipo; 

      var panel_error_heading = document.createElement('div');
      panel_error_heading.className = "panel-heading";
      panel_error_heading.textContent = mensaje.heading;

      var panel_error_body = document.createElement('div');
      panel_error_body.className = "panel-body";
      panel_error_body.textContent = mensaje.body;

      panel_error.appendChild(panel_error_heading);
      panel_error.appendChild(panel_error_body);
      ventana.appendChild(panel_error);
    }

    var _generarVentanaTabla = function() {
      var ventana = document.getElementById('ventanaTabla');
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (this.readyState === 4) {
          if (this.status === 200) {
            var documento_xml = this.responseXML;
            if (ventana.firstChild) {
              ventana.removeChild(ventana.firstChild);
            }
            /* <table> */
            var div = document.createElement("div");
            div.className = "table-responsive";
            var tabla = document.createElement("table"); 
            var caption = document.createElement("caption");
            caption.textContent = 'Books';
            tabla.appendChild(caption);
      
            /* Twitter Bootstrap */
            tabla.className = "table table-striped table-bordered 
table-hover table-condensed";
      
            /* <thead> */
            var campos = ['#', 'Author', 'Title', 'Genre', 'Price', 
'Publish date', 'Description', 'Options'];
            var tabla_thead = document.createElement("thead");
            var tabla_tr = document.createElement("tr");
            for (var i = 0; i < campos.length; i++ ) {
              var tabla_th = document.createElement("th");
              tabla_th.textContent = campos[i];
              tabla_tr.appendChild(tabla_th);
            }
            tabla_thead.appendChild(tabla_tr);
          
            /* <tbody> */
            var tabla_tbody = document.createElement("tbody");
            /* podrÃ¡ ser llamada por cualquiera de sus "hijos"... el 
botÃ³n DELETE */
            tabla_tbody.actualizarContador = function() {
              var contador = 0;
              var trs = this.childNodes;
              for (var i=0; i < trs.length; i++) {
                trs[i].firstChild.textContent = ++contador;
              }
            };
            var xml_nodos = documento_xml.firstChild.childNodes;
            for (var i=0, contador = 0; i < xml_nodos.length; i++) {
              if (xml_nodos[i].nodeType !== 1) {
                continue;
              }
              var tabla_tr = document.createElement("tr");

              /* contador */
              var tabla_contador = document.createElement("td");
              tabla_contador.textContent = ++contador;
              tabla_tr.appendChild(tabla_contador);
      
              /* datos del libro */
              var nodo_elemento = xml_nodos[i].childNodes;
              for (var j=0; j < nodo_elemento.length; j++) {
                var tabla_td = document.createElement("td");
                if (nodo_elemento[j].nodeType === 1) {
                  if (nodo_elemento[j].nodeName === "title") {
                    titulo_libro = nodo_elemento[j].textContent;
                  }
                  tabla_td.textContent = nodo_elemento[j].textContent;
                  tabla_tr.appendChild(tabla_td);
                }
              }

              /* id del libro */
              var id_libro;
              var book_atributos = xml_nodos[i].attributes;
              for (var a=0; a < book_atributos.length; a++) {
                if (book_atributos[a].nodeName === "id") {
                  id_libro = book_atributos[a].nodeValue;
                }
              }

              /* opciones */
              var tabla_td = document.createElement("td");
              /* botÃ³n DELETE */
              var boton_delete = document.createElement('button');
              boton_delete.textContent = 'DELETE';
              boton_delete.className = "btn btn-warning btn-xs 
btn-block";
              boton_delete.recurso = _RECURSO + "/" + id_libro;
              boton_delete.id_libro = id_libro;
              boton_delete.titulo_libro = titulo_libro;
              boton_delete.addEventListener('click', function(boton) {
                return function() {
                  var xhr = new XMLHttpRequest();
                  xhr.onreadystatechange = function (boton) {
                    return function() {
                      if (this.readyState === 4) {
                        if (this.status === 200) {
                          var mensaje = {
                            'tipo': 'panel-success',
                            'heading': 'Exito',
                            'body': 'El libro con id ' + boton.id_libro 
+ ' y tÃ­tulo "' + boton.titulo_libro + '" ha sido borrado'
                          };
                          _mostrarMensaje(mensaje);
                          /* para borrar *localmente* al libro... en el 
servidor no se accede a la base de datos */
                          var tbody = 
boton.parentNode.parentNode.parentNode;
                          
boton.parentNode.parentNode.parentNode.removeChild(boton.parentNode.parentNode);
                          tbody.actualizarContador();
                        } else {
                          var mensaje = {
                            'tipo': 'panel-warning',
                            'heading': 'Error' + ' ' + this.status,
                            'body': this.statusText
                          };
                          _mostrarMensaje(mensaje);
                          /* para indicar con que libro se tuvo algÃºn 
error */
                          boton.disabled = true;
                          boton.className += " btn-default";
                          boton.parentNode.parentNode.className = 
"warning";
                        }
                      }
                    };
                  }(boton);
                  xhr.open('DELETE', boton.recurso);
                  xhr.send();
                };
              }(boton_delete), false);
              tabla_td.appendChild(boton_delete);
              tabla_tr.appendChild(tabla_td);

              tabla_tbody.appendChild(tabla_tr);
            }

            tabla.appendChild(tabla_thead);
            tabla.appendChild(tabla_tbody);
            div.appendChild(tabla);
            ventana.appendChild(div);
          } else {
            console.log('Â¡Error!');
          }
        }
      }
      xhr.open('GET', _RECURSO);
      xhr.send();
    }
    var _crearVentanas = function() {
      _generarVentanaTabla();
    }
    var _iniciar = function() {
      _crearVentanas();
    }
    return {
      "main": _iniciar
    };
  }
)();
