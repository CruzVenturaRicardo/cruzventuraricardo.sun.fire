<!DOCTYPE html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>API</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="../public/style.css" rel="stylesheet" type="text/css" media="screen" />
<script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body >
  
<a href="/">Regresar</a>
    <h1>Recursos</h1>
<ul>
  <li>
<a href="/api/pib"> /api/pib</a>
ejemplo: <a href="/api/pib"> /api/pib</a>

  </li>
  <li>
<a href="/api/pib/indicador"> /api/pib/indicador</a>
ejemplo: <a href="/api/pib/1008000012"> /api/pib/1008000012</a>
  </li>
  <li>
<a href="/api/pib/entidad/clave"> /api/pib/entidad/clave</a>
ejemplo: <a href="/api/pib/entidad/00"> /api/pib/entidad/00</a>
  </li>
  <li>
<a href="/api/entidad?desc='descripción'"> /api/entidad?desc="descripción"</a>
  </li>
  <li>
<a href="/api/entidad/clave?desc='nombreEntidad'"> /api/entidad/clave?desc="nombreEntidad"</a>
  </li>
  <li>
<a href="/api/indicador/id"> /api/indicador/id</a>
  </li>
</ul>
<p>Para información sobre cómo hacer uso de la API, por favor acceda a la <a href="../doc">Documentación</a></p>
  </body>
</html>

